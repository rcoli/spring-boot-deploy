#!/bin/bash

# COMMAND LINE VARIABLES
#enviroment FIRST ARGUMENT
env=$1
# deploy port SECOND ARGUMENT
serverPort=$2


#### CONFIGURABLE VARABLES ######
#destination absolute path They must be pre created
projectName=rede-clickpag-bankline-api
configFileName=application-leon.yml

destAbsPath=/home/rede/projetos/$projectName/$env
configFolder=artifacts

#jar file
sourFile=$WORKSPACE/bankline-api-project/build/libs/$projectName*.jar
destFile=$destAbsPath/$projectName.jar

#config files folder
sourConfigFolder=$WORKSPACE/$configFolder*
destConfigFolder=$destAbsPath/$configFolder

properties=--spring.config.location=$destAbsPath/artifacts/$configFileName

#CONSTANTS
logFile=initServer.log
dstLogFile=$destAbsPath/$logFile
whatToFind="Started Application in"
msgLogFileCreated="$logFile created"
msgBuffer="Buffering: "
msgAppStarted="Application Started... exiting buffer!"

### FUNCTIONS
##############
function stopServer(){
    echo " "
    echo "Stoping process on port: $serverPort"
    fuser -n tcp -k $serverPort > redirection &
    echo " "
}

function deleteFiles(){
    echo "Deleting $destFile"
    rm -rf $destFile

    echo "Deleting $destConfigFolder"
    rm -rf $destConfigFolder

    echo "Deleting $dstLogFile"
    rm -rf $dstLogFile

    echo "Deleting Log Folder $destAbsPath/logs"
    rm -rf -r $destAbsPath/logs
    
    echo " "
}

function copyFiles(){
    echo "Copying files from $sourFile"
    cp $sourFile $destFile

    echo "Copying files from $sourConfigFolder"
    cp -r $sourConfigFolder $destConfigFolder

    echo " "
}

function run(){
   echo "java -jar $destFile --server.port=$serverPort $properties" | at now + 1 minutes

   echo "COMMAND: echo \"java -jar $destFile --server.port=$serverPort $properties\" | at now + 1 minutes\""

    echo " "
}
function changeFilePermission(){
    chmod 755 $destFile
}

function watch(){
 
    tail -f $dstLogFile |

        while IFS= read line
            do
                echo "$msgBuffer" "$line"

                if [[ "$line" == *"$whatToFind"* ]]; then
                    echo $msgAppStarted
                    pkill  tail
                fi
        done
}

### FUNCTIONS CALLS
#####################
# Use Example of this file.
# In case of timeout, jenkins will mark as build failure
# timeout 120 this-file-name.sh

# 1 - stop server on port ...
stopServer

# 2 - delete destinations folder content
deleteFiles

# 3 - copy files to deploy dir
copyFiles

changeFilePermission

# 4 - start server
run

# 5 - watch loading messages until  ($whatToFind) message is found
#watch &
